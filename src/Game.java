import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;

public class Game extends JFrame implements KeyListener {

	class Snake {

		public Node head;
		public ArrayList<Node> body = new ArrayList<>();

		public Node direction = new Node(-1, 0);

		public Snake(int x, int y) {
			born(x, y);
		}

		public void moveTo(Node direction) {
			if (direction == null)
				return;
			updateBody(head);
			head.x += direction.x;
			head.y += direction.y;
		}

		public void updateBody(Node startNode) {
			Node nextPosition = new Node(startNode);
			Node prevPosition = new Node();
			for (Node bodyPart : body) {
				prevPosition.copy(bodyPart);
				bodyPart.copy(nextPosition);
				nextPosition.copy(prevPosition);
			}
		}

		public void turnX(int x) {
			direction.x = x;
		}

		public void turnY(int y) {
			direction.y = y;
		}

		public void born() {
			head = new Node(0, 0);
		}

		public void born(int x, int y) {
			head = new Node(x, y);
		}

		public void eat() {
			body.add(new Node(head));
		}

		public void tryChangeDir(Node newDirection) {
			if (direction.x != -newDirection.x || direction.y != -newDirection.y) {
				direction.copy(newDirection);
			}
		}

	}

	class Pixel {
		public Color color = new Color(0);
		private JLabel label;

		public Pixel(JLabel label) {
			this.label = label;
		}

		public void paint(Color color) {
			if (color.getRGB() == this.color.getRGB())
				return;

			this.color = color;
			label.setBackground(this.color);
		}
	}

	class Node {
		int x;
		int y;

		public Node() {
			this.x = 0;
			this.y = 0;
		}

		public Node(Node source) {
			this.x = source.x;
			this.y = source.y;
		}

		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public void copy(Node node) {
			this.x = node.x;
			this.y = node.y;
		}

		public boolean isEquivalent(Node node) {
			return node != null && node.x == x && node.y == y;
		}
	}

	private JPanel contentPane;
	private Snake snake;
	private Node fruit;

	private Pixel[] pixels;
	private int size = 20;

	private final int RIGHT = 68;
	private final int LEFT = 65;
	private final int UP = 87;
	private final int DOWN = 83;

	public int waitTime = 1000;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();

		Game frame = new Game();
		frame.setVisible(true);
		frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				ses.shutdown();
			}

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		ses.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					frame.refresh();
				} catch (Exception e) {
					e.printStackTrace();
					frame.debug();
				}
			}
		}, 0, 100, TimeUnit.MILLISECONDS);

	}

	protected void debug() {
System.err.println(this.snake.head.y);
		
	}

	public void refresh() {
		doGameLogic();
		if (isVisible())
			doRender();

		doLayout();
	}

	public void doGameLogic() {
		snake.moveTo(snake.direction);
		
		// Collision testing
		for(Node bodyPart : snake.body) {
			if (snake.head.isEquivalent(bodyPart))
			{
				this.dispose();
				return;
			}
		}
		
		if 	(snake.head.x < 0 
			|| snake.head.x >= size 
			|| snake.head.y < 0 
			|| snake.head.y >= size
		) {
			this.dispose();
			return;
		}
		
		if (snake.head.isEquivalent(fruit)) {
			snake.eat();
			fruit.copy(generateApplePosition());
			waitTime-=100;
		}
	}

	public void doRender() {

		// Defino los colores
		Color blank = new Color(255, 255, 255);
		Color snakeHead = new Color(104, 215, 36);
		Color[] snakeBody = { new Color(68, 129, 47), new Color(99, 159, 71) };
		Color snakeTail = new Color(41, 72, 26);
		Color fruitColor = new Color(222, 100, 10);

		// Pinto el mundo
		for (var pixel : pixels) {
			pixel.paint(blank);
		}

		// Obtengo el nodo principal
		Node head = snake.head;

		// Pinto la serpiente
		pixels[getIndex(head)].paint(snakeHead);

		int index = 0;
		for (Node bodyPart : snake.body) {
			int colorIndex = index % snakeBody.length;
			pixels[getIndex(bodyPart)].paint(snakeBody[colorIndex]);
			index++;
		}

		// Pinto la cola
		Node tail = snake.body.get(snake.body.size() - 1);
		pixels[getIndex(tail)].paint(snakeTail);

		// Pinto la fruta
		pixels[getIndex(fruit)].paint(fruitColor);
	}

	public int getIndex(Node node) {
		return node.y * size + node.x;
	}

	public Node generateApplePosition() {

		Random rng = new Random();
		int x = (int) Math.floor(rng.nextFloat() * size);
		int y = (int) Math.floor(rng.nextFloat() * size);

		return new Node(x, y);
	}

	/**
	 * Create the frame.
	 */
	public Game() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		addKeyListener(this);

		snake = new Snake((int) Math.round(size / 2), (int) Math.round(size / 2));

		snake.eat();
		snake.eat();
		snake.eat();

		fruit = new Node(generateApplePosition());

		contentPane.setLayout(new GridLayout(size, size, 0, 0));

		pixels = new Pixel[size * size];

		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				JLabel label = new JLabel();
				label.setHorizontalAlignment(JLabel.CENTER);
				label.setVerticalAlignment(JLabel.CENTER);
				label.setOpaque(true);
				Pixel pixel = new Pixel(label);
				pixels[y * size + x] = pixel;
				contentPane.add(label);
			}
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

		Node newDirection = new Node(0, 0);

		switch (e.getKeyCode()) {
		case LEFT:
			newDirection.x = -1;
			newDirection.y = 0;
			break;
		case RIGHT:
			newDirection.x = 1;
			newDirection.y = 0;
			break;
		case UP:
			newDirection.x = 0;
			newDirection.y = -1;
			break;
		case DOWN:
			newDirection.x = 0;
			newDirection.y = 1;
			break;
		default:
			newDirection.copy(snake.direction);
			break;
		}

		snake.tryChangeDir(newDirection);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
